'use strict';

const path = require('path');

module.exports = {
  'process/browser': path.resolve(__dirname, '../../../node_modules', 'process/browser'),
  '@uikit-vue-components': path.resolve(__dirname, '../../../src/components/'),
  '@fonts': path.resolve(__dirname, '../../../assets/fonts'),
  '@images': path.resolve(__dirname, '../../../assets/images'),
  'timcowebapps/common.utils': path.resolve(__dirname, '../../../node_modules', '@timcowebapps/common.utils'),
  'timcowebapps/common.ooscss': path.resolve(__dirname, '../../../node_modules', '@timcowebapps/common.ooscss')
};
