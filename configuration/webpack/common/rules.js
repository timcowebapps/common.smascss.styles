'use strict';

const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const cssloader = function (mode, target, importLoaders) {
  var loaders = [];

  if (target === 'web')
    loaders.push((mode === 'development')
      ? { loader: 'style-loader' }
      : { loader: MiniCssExtractPlugin.loader }
    );

  loaders.push({
    loader: 'css-loader',
    options: {
      importLoaders,
      sourceMap: true
    }
  });

  return loaders;
};

module.exports = function (mode, target) {
  return [
    // TS
    {
      test: /\.ts$/,
      loader: 'ts-loader',
      options: { appendTsSuffixTo: [/\.vue$/] }
    },
    // SCSS|CSS
    {
      test: /\.(scss|css)$/,
      resolve: { extensions: ['.scss', '.css'] },
      use: [
        ...cssloader(mode, target, 2),
        {
          loader: 'postcss-loader',
          options: { sourceMap: true }
        },
        {
          loader: 'sass-loader',
          options: {
            sourceMap: true,
            warnRuleAsWarning: true,
            // data: `
            //   @import "@/scss/_variables.scss";
            //   @import "@/scss/_mixins.scss";
            // `
          }
        }
      ]
    },
    // ASSETS
    {
      test: /\.(eot|svg|ttf|woff|woff2)$/,
      loader: 'file-loader',
      options: {
        publicPath: path.join(__dirname, '../../../assets/fonts/'),
        outputPath: '../assets/fonts/'
      }
    }
  ]
};
