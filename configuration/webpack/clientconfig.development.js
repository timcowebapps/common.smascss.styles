'use strict';

const _ = require('lodash');
const path = require('path');
const webpack = require('webpack');
const { merge } = require('webpack-merge');
const HtmlPlugin = require('html-webpack-plugin');
const buildConfg = require('../buildconfig.js');

module.exports = merge(require('./clientconfig.base.js')('development'), {
  name: 'ClientDevConfg',
  mode: 'development',
  devtool: 'cheap-module-source-map',
  entry: {
    'client': [
      `webpack-dev-server/client?http://${buildConfg.dev.hostname}:${buildConfg.dev.port}`,
      'webpack/hot/only-dev-server',
      //path.resolve(__dirname, '../../', buildConfg.paths.src.base, 'main')
      path.resolve(__dirname, '../../', "src/shared", 'master')
    ]
  },
  output: {
    filename: buildConfg.disabledChunkhash
      ? '[name].js'
      : '[name].[fullhash:8].js',
    sourceMapFilename: buildConfg.disabledChunkhash
      ? '[file].js.map'
      : '[file].[fullhash:8].map',
    chunkFilename: buildConfg.disabledChunkhash
      ? '[name].js'
      : '[name].[fullhash:8].chunk.js',
    hotUpdateChunkFilename: 'hot/[id].hot-update.js',
    hotUpdateMainFilename: 'hot/hot-update.json',
    publicPath: buildConfg.dev.publicPath
  },
  plugins: [
    new webpack.DefinePlugin({
      //'process.platform': 'darwin',
      'process.env': {
        NODEENV: JSON.stringify('development'),
        BROWSER: JSON.stringify(true),
        PORT: buildConfg.dev.port
      }
    }),
    new HtmlPlugin({
      title: '',
      template: path.resolve(__dirname, '../../src/shared/', buildConfg.tmpl.master + buildConfg.tmpl.extname),
      filename: path.resolve(__dirname, '../../', buildConfg.paths.output.base, buildConfg.tmpl.master + buildConfg.tmpl.extname),
      minify: false,
      hash: false,
      inject: false,
      urlContent: (content) => content
    }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.ProvidePlugin({
      Buffer: ['buffer', 'Buffer'],
      process: 'process/browser'
    })
  ]
});
