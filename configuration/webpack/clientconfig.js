'use strict';

const path = require('path');
const webpack = require('webpack');
const { merge } = require('webpack-merge');
const HtmlPlugin = require('html-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const { buildConfg, SERVER_PORT } = require('../buildconfig.js');

module.exports = merge(require('./clientconfig.base.js')('production'), {
  name: 'ClientConfig',
  mode: 'production',
  devtool: 'inline-source-map',
  node: {
    //fs: 'empty'
  },
  entry: {
    'client': {
      import: path.resolve(__dirname, '../../', buildConfg.paths.src.base, 'main')
    }
  },
  output: {
    filename: buildConfg.disabledChunkhash
      ? '[name].js'
      : '[name].[fullhash:8].js',
    sourceMapFilename: buildConfg.disabledChunkhash
      ? '[file].map'
      : '[file].[fullhash:8].map',
    chunkFilename: buildConfg.disabledChunkhash
      ? '[name].chunk.js'
      : '[name].[fullhash:8].chunk.js',
    publicPath: buildConfg.urls.publicPath
  },
  optimization: {
    chunkIds: 'named',
    splitChunks: {
      minChunks: 1,
      cacheGroups: {
        vendors: {
          name: 'chunk-vendors',
          test: /[\\/]node_modules[\\/]/,
          priority: -10,
          chunks: 'initial'
        },
        common: {
          name: 'chunk-common',
          minChunks: 2,
          priority: -20,
          chunks: 'initial',
          reuseExistingChunk: true
        },
        button: {
          name: 'chunk-buttoncontrol',
          test: /[\\/]src[\\/]client[\\/]components[\\/]widgets[\\/]button[\\/]/,
          chunks: 'all',
          enforce: true
        },
        input: {
          name: 'chunk-input',
          test: /[\\/]src[\\/]client[\\/]components[\\/]widgets[\\/]input[\\/]/,
          chunks: 'all',
          enforce: true
        },
        select: {
          name: 'chunk-select',
          test: /[\\/]src[\\/]client[\\/]components[\\/]widgets[\\/]select[\\/]/,
          chunks: 'all',
          enforce: true
        },
        table: {
          name: 'chunk-tablewidget',
          test: /[\\/]src[\\/]client[\\/]components[\\/]widgets[\\/]table[\\/]/,
          chunks: 'all',
          enforce: true
        }
      }
    },
    minimize: true,
    minimizer: [
      new TerserPlugin({
        parallel: true
      })
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: buildConfg.disabledChunkhash
        ? '[name].css'
        : '[name].[contenthash:8].css',
      chunkFilename: buildConfg.disabledChunkhash
        ? '[name].chunk.css'
        : '[name].[contenthash:8].chunk.css'
    }),
    new webpack.DefinePlugin({
      //'process.platform': 'darwin',
      'process.env': {
        NODEENV: JSON.stringify('production'),
        BROWSER: JSON.stringify(true),
        PORT: buildConfg.dev.port
      }
    }),
    new HtmlPlugin({
      title: '',
      template: path.resolve(__dirname, '../../src/shared/', buildConfg.tmpl.master + buildConfg.tmpl.extname),
      filename: path.resolve(__dirname, '../../', buildConfg.paths.output.base, buildConfg.tmpl.master + buildConfg.tmpl.extname),
      hash: false,
      cache: true,
      showErrors: false,
      inject: false,
      injectState: {
        INITIAL_STATE: `<script id='__INITIAL_STATE__' type='application/json'>
        </script>`
      },
      urlContent: (content) => `http://127.0.0.1:${buildConfg.dev.port}/` + content,
      minify: false
    }),
    new CompressionPlugin({
      test: /\.js$|\.css$|\.html$/,
      filename: '[path][base].gz',
      algorithm: 'gzip',
      threshold: 10240,
      minRatio: 0.8
    })
  ]
});
