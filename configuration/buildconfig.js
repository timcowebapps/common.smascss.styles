'use strict';

const SERVER_PROTOCOL = 'http';
const SERVER_HOSTNAME = '127.0.0.1';
const SERVER_PORT = 8085;

module.exports = {
  isDebugMode: true,
  isDevelopmentMode: (process.env.NODE_ENV || 'development').trim().toLowerCase() === 'development',
  dev: {
    hostname: SERVER_HOSTNAME,
    port: SERVER_PORT,
    publicPath: `${SERVER_PROTOCOL}://${SERVER_HOSTNAME}:${SERVER_PORT}/dist/`
  },
  disabledChunkhash: true,
  tmpl: {
    master: 'index',
    extname: '.html'
  },
  paths: {
    src: {
      base: './src/',
      views: './src/shared/'
    },
    output: {
      base: './wwwroot/',
      dist: './wwwroot/dist/',
      assets: './wwwroot/assets/',
      fonts: './wwwroot/assets/fonts/'
    }
  },
  urls: {
    publicPath: './dist/'
  }
};
