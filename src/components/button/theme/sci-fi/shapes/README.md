```ts
import HexagonShapeSvg from 'timcowebapps/common.smascss.styles/src/components/button/theme/sci-fi/shapes/hexagon.svg';

export default defineComponent({
  components: {
    HexagonShapeSvg
  }
});
```

```html
<div :class='cnstr(`[{ "block": "hexagon-clip-block" }]`)'>
  <div :class='cnstr(`[{ "block": "hexagon-clip-each" }, { "block": "hexagon-clip-solid" }]`)'>
    <div :class='cnstr(`[{ "block": "hexagon-clip-caption" }]`)'>
      work
    </div>
  </div>
</div>

<HexagonShapeSvg />
```
