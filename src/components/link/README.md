## Компонент ссылки.

| Свойство | Тип | Значение по умолчанию | Описание |
| :--- | :--- | :--- | :--- |
| `$link_name` | `string` | `link` | Имя компонента |
| `$link-color` | `color` | `#333` | Цвет текста |
| `$link-color--hover` | `color` | `#333` | Цвет текста при наведении курсора мышки |
| `$link_font_weight` | `-` | `700` | Начертание шрифта |
| `$link_line_height` | `number` | `1.25` | Межстрочное расстояние |
| `$link_text_decoration` | `-` | `underline` | Оформление текста |
| `$link_text_transform` | `-` | `underline` | Преобразование текста |
| `$link_background` | `color` | `transparent` | Цвет фона |
| `$link_background_hover` | `color` | `transparent` | Цвет фона при наведении курсора мышки |
| `$link_border_width` | `-` | `0 0 1px` | Толщина границы |
| `$link_border_style` | `-` | `dotted` | Стиль границы вокруг элемента |
| `$link_border_color` | `color` | `transparent` | Цвет рамки |
| `$link_border_color_hover` | `color` | `transparent` | Цвет рамки при наведении курсора мышки |
| `$link_padding` | `number` | `0` | Внутренний отступ |
