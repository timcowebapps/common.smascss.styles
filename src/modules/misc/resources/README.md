```scss
@use "../../misc/resources/color-state" as color_state_obj;

// Пустая карта под состояния
$object_state_map: ();

// Создаем новое состояние и сохраняем его в $object_state_map
$object_state_map: color_state_obj.new_state($object_state_map, "norm");
// Устанавливаем новое значение цвета в ранее созданное состояние
$object_state_map: color_state_obj.set_value($object_state_map, "norm", "base", #FF0);

// Создаем новое состояние со значение цветов
$object_state_map: color_state_obj.new_state($object_state_map, "hover", ("red": #F00, "blue": #00F));
```