'use strict';

const path = require('path');
const webpack = require('webpack');
const WebpackDevServer = require('webpack-dev-server');
const buildConfg = require('../../configuration/buildconfig.js');
const configClient = require('../../configuration/webpack/clientconfig.development.js');

const devServerOptions = {
  https: false,
  devMiddleware: {
    publicPath: buildConfg.dev.publicPath
  },
  static: {
    directory: path.join(__dirname, '../../', buildConfg.paths.output.base),
    watch: true
  },
  hot: true,
  host: buildConfg.dev.hostname,
  port: buildConfg.dev.port,
  compress: true,
  open: false,
  historyApiFallback: true,
  allowedHosts: 'all',
  headers: { 'Access-Control-Allow-Origin': '*' }
};

const compiler = webpack(configClient);
const devServer = new WebpackDevServer(devServerOptions, compiler);
(async () => {
  await devServer.start();
  console.log('Webpack server launched with at <localhost>:%d (Hot Module Replacement [HMR] enabled)', buildConfg.dev.port);
})();
